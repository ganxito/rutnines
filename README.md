##Add this to .barsh, permanent alias
#use: permalias clr=clear
function permalias ()
{
  alias "$*";
  echo alias "$*" >> ~/.bash_aliases
}

